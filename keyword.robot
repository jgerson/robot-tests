*** Settings ***
Documentation     Example test cases using the keyword-driven testing approach.
Library           CSWrapperLibrary.py

*** Variables ***
${RESERVATIONID}  %{CLOUDSHELL_RESERVATION_ID}
${SERVERADDRESS}  %{CLOUDSHELL_SERVER_ADDRESS}
${ADMINUSER}  %{CLOUDSHELL_USERNAME}
${ADMINPW}  %{CLOUDSHELL_PASSWORD}
${ADMINDOMAIN}  %{CLOUDSHELL_DOMAIN}

*** Test Cases ***
Run command on resources
    register cloudshell     ${RESERVATIONID}     ${SERVERADDRESS}    ${ADMINUSER}    ${ADMINPW}  ${ADMINDOMAIN}
    Run resource command    Dummy    HelloWorld     
    result_should_contain   World